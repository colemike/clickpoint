using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using ClickPoint.Controllers;
using ClickPoint.Data;
using ClickPoint.Domain.Models;
using ClickPoint.Infrastructure.Authentication;
using Moq;
using Xunit;

namespace ClickPoint.Tests.Controllers
{
    public class LeadControllerTests
    {
        [Fact]
        public void LeadController_NewLead_RetrievesLeadWhenExists()
        {
            //Arrange
            var data = new List<Lead>
            {
                new Lead { FirstName="foo", LastName = "bar"}
            };

            var set = new Mock<DbSet<Lead>>()
                .SetupData(data);

            var context = new Mock<DataContext>();
            context.Setup(c => c.Leads).Returns(set.Object);

            var authenticator = new Mock<IAuthenticator>();

            var controller = new LeadController(context.Object, authenticator.Object);

            //Act
            var result = (ContentResult)controller.NewLead();

            //Assert
            Assert.Equal("New Lead: foo bar", result.Content);
        }

        [Fact]
        public void LeadController_NewLead_ReturnsMessageWhenNoLeads()
        {
            //Arrange
            var set = new Mock<DbSet<Lead>>()
                .SetupData(new List<Lead>());

            var context = new Mock<DataContext>();
            context.Setup(c => c.Leads).Returns(set.Object);

            var authenticator = new Mock<IAuthenticator>();

            var controller = new LeadController(context.Object, authenticator.Object);

            //Act
            var result = (ContentResult)controller.NewLead();

            //Assert
            Assert.Equal("No new leads available.", result.Content);
        }

        [Fact]
        public void LeadController_NewLead_DoesNotReturnAssignedLead()
        {
            //Arrange
            var data = new List<Lead>
            {
                new Lead { FirstName="foo", LastName = "bar", Processor = new User("","","","")}
            };

            var set = new Mock<DbSet<Lead>>()
                .SetupData(data);

            var context = new Mock<DataContext>();
            context.Setup(c => c.Leads).Returns(set.Object);

            var authenticator = new Mock<IAuthenticator>();

            var controller = new LeadController(context.Object, authenticator.Object);

            //Act
            var result = (ContentResult)controller.NewLead();

            //Assert
            Assert.Equal("No new leads available.", result.Content);
        }

        [Fact]
        public void LeadController_NewLead_RetrievesOldestLeadFirst()
        {
            //Arrange
            var data = new List<Lead>
            {
                new Lead { FirstName="foo", LastName = "bar", EnteredOn = DateTime.Now },
                new Lead { FirstName="baz", LastName = "blah", EnteredOn = DateTime.Now.AddDays(-10) },
                new Lead { FirstName="bam", LastName = "pow", EnteredOn = DateTime.Now.AddDays(1) },
            };

            var set = new Mock<DbSet<Lead>>()
                .SetupData(data);

            var context = new Mock<DataContext>();
            context.Setup(c => c.Leads).Returns(set.Object);

            var authenticator = new Mock<IAuthenticator>();

            var controller = new LeadController(context.Object, authenticator.Object);

            //Act
            var result = (ContentResult)controller.NewLead();

            //Assert
            Assert.Equal("New Lead: baz blah", result.Content);
        }

        [Fact]
        public void LeadController_NewLead_AssignsRetrievedLead()
        {
            //Arrange
            var lead = new Lead {FirstName = "foo", LastName = "bar"};
            var data = new List<Lead>
            {
                lead
            };

            var set = new Mock<DbSet<Lead>>()
                .SetupData(data);

            var context = new Mock<DataContext>();
            context.Setup(c => c.Leads).Returns(set.Object);

            var currentUser = new User("foo", "bar", "baz", "blah");
            var authenticator = new Mock<IAuthenticator>();
            authenticator.Setup(auth => auth.GetCurrentUser()).Returns(currentUser);

            var controller = new LeadController(context.Object, authenticator.Object);

            //Act
            controller.NewLead();

            //Assert
            Assert.Equal(currentUser, lead.Processor);
            context.Verify(c => c.SaveChanges());
        }
    }
}