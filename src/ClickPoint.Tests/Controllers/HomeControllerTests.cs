﻿using ClickPoint.Controllers;
using Xunit;

namespace ClickPoint.Tests.Controllers
{
    public class HomeControllerTests
    {
        [Fact]
        public void HomeController_Index_ReturnsAView()
        {
            //Arrange
            var controller = new HomeController();

            //Act
            var result = controller.Index();

            //Assert
            Assert.NotNull(result);
        }
    }
}
