using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using ClickPoint.Controllers;
using ClickPoint.Data;
using ClickPoint.Domain.Models;
using ClickPoint.Infrastructure.Authentication;
using ClickPoint.Models.Account;
using Moq;
using Xunit;

namespace ClickPoint.Tests.Controllers
{
    public class AccountControllerTests
    {
        [Fact]
        public void AccountController_Login_ReturnsAView()
        {
            //Arrange
            var authenticator = new Mock<IAuthenticator>();
            var context = new Mock<DataContext>();
            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (ViewResult)controller.Login();

            //Assert
            Assert.IsAssignableFrom<LoginViewModel>(result.ViewData.Model);
        }

        [Fact]
        public void AccountController_Login_ReturnsMessageWhenUserIsCreated()
        {
            //Arrange
            var authenticator = new Mock<IAuthenticator>();
            var context = new Mock<DataContext>();
            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (ViewResult)controller.Login(true);

            //Assert
            var model = Assert.IsAssignableFrom<LoginViewModel>(result.ViewData.Model);
            Assert.True(model.WasUserCreated);
        }

        [Fact]
        public void AccountController_Login_ReturnsMessageWhenLoginFails()
        {
            //Arrange
            const string username = "foo";
            const string password = "bar";

            var authenticator = new Mock<IAuthenticator>();
            authenticator.Setup(auth => auth.Verify(username, password)).Returns(false);

            var context = new Mock<DataContext>();

            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (ViewResult)controller.Login(new LoginViewModel { Username = "fail", Password = "fail" }, null);

            //Assert
            var model = Assert.IsAssignableFrom<LoginViewModel>(result.ViewData.Model);
            Assert.True(model.HasLoginFailed);
        }

        [Fact]
        public void AccountController_Login_RedirectsToHomeWhenLoginSucceeds()
        {
            //Arrange
            const string username = "foo";
            const string password = "bar";

            var authenticator = new Mock<IAuthenticator>();
            authenticator.Setup(auth => auth.Verify(username, password)).Returns(true);

            var context = new Mock<DataContext>();

            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (RedirectToRouteResult)controller.Login(new LoginViewModel { Username = username, Password = password }, null);

            //Assert
            authenticator.Verify(auth => auth.Authenticate(username, password));
            Assert.Equal("Index", result.RouteValues["action"]);
            Assert.Equal("Home", result.RouteValues["controller"]);
        }

        [Fact]
        public void AccountController_Login_RedirectsToCustomUrlWhenLoginSucceeds()
        {
            //Arrange
            const string username = "foo";
            const string password = "bar";

            var authenticator = new Mock<IAuthenticator>();
            authenticator.Setup(auth => auth.Verify(username, password)).Returns(true);

            var context = new Mock<DataContext>();

            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (RedirectResult)controller.Login(new LoginViewModel { Username = username, Password = password }, "foo");

            //Assert
            authenticator.Verify(auth => auth.Authenticate(username, password));
            Assert.Equal("foo", result.Url);
        }

        [Fact]
        public void AccountController_Logout_LogsOut()
        {
            //Arrange
            var authenticator = new Mock<IAuthenticator>();
            var context = new Mock<DataContext>();
            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (RedirectToRouteResult)controller.LogOut();

            //Assert
            authenticator.Verify(auth => auth.Logout());
            Assert.Equal("Login", result.RouteValues["action"]);
        }

        [Fact]
        public void AccountController_Add_ReturnsAView()
        {
            //Arrange
            var authenticator = new Mock<IAuthenticator>();
            var context = new Mock<DataContext>();
            var controller = new AccountController(authenticator.Object, context.Object);

            //Act
            var result = (ViewResult)controller.Add();

            //Assert
            Assert.IsAssignableFrom<AddAccountViewModel>(result.ViewData.Model);
        }

        [Fact]
        public void AccountController_Add_ReportsErrorOnDuplicateUsername()
        {
            //Arrange
            var data = new List<User>
            {
                new User("foo", "bar", "baz", "blah")
            };

            var set = new Mock<DbSet<User>>()
                .SetupData(data);

            var context = new Mock<DataContext>();
            context.Setup(c => c.Users).Returns(set.Object);

            var authenticator = new Mock<IAuthenticator>();

            var controller = new AccountController(authenticator.Object, context.Object);
            controller.ViewData.ModelState.Clear();

            //Act
            var result = (ViewResult)controller.Add(new AddAccountViewModel { Username = "foo" });

            //Assert
            Assert.IsAssignableFrom<AddAccountViewModel>(result.ViewData.Model);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Username"));
        }

        [Fact]
        public void AccountController_Add_ReportsErrorOnDuplicateEmail()
        {
            //Arrange
            var data = new List<User>
            {
                new User("foo", "bar", "baz", "blah")
            };

            var set = new Mock<DbSet<User>>()
                .SetupData(data);

            var context = new Mock<DataContext>();
            context.Setup(c => c.Users).Returns(set.Object);

            var authenticator = new Mock<IAuthenticator>();

            var controller = new AccountController(authenticator.Object, context.Object);
            controller.ViewData.ModelState.Clear();

            //Act
            var result = (ViewResult)controller.Add(new AddAccountViewModel { Email = "bar" });

            //Assert
            Assert.IsAssignableFrom<AddAccountViewModel>(result.ViewData.Model);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Email"));
        }

        [Fact]
        public void AccountController_Add_SuccessfullyAddsUser()
        {
            //Arrange
            var set = new Mock<DbSet<User>>()
                .SetupData(new List<User>());

            var context = new Mock<DataContext>();
            context.Setup(c => c.Users).Returns(set.Object);


            var authenticator = new Mock<IAuthenticator>();

            var controller = new AccountController(authenticator.Object, context.Object);
            controller.ViewData.ModelState.Clear();

            //Act
            var result = (RedirectToRouteResult)controller.Add(new AddAccountViewModel { Username = "foo", Email = "bar", FirstName = "baz", LastName = "blah", Password = "what" });

            //Assert
            Assert.Equal(1, set.Object.Count());
            var newObject = set.Object.ElementAt(0);
            Assert.Equal("foo", newObject.UserName);
            Assert.Equal("bar", newObject.Email);
            Assert.Equal("baz", newObject.FirstName);
            Assert.Equal("blah", newObject.LastName);

            context.Verify(con => con.SaveChanges());
            authenticator.Verify(auth => auth.CreateAccount("foo", "what"));

            Assert.Equal("Login", result.RouteValues["action"]);
        }
    }
}