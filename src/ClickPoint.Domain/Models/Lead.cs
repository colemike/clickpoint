using System;

namespace ClickPoint.Domain.Models
{
    public class Lead : KeyedEntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Comments { get; set; }
        public DateTime EnteredOn { get; set; }
        public LeadStatus? Status { get; set; }

        public virtual User Processor { get; set; }
    }
}