﻿namespace ClickPoint.Domain.Models
{
    public abstract class KeyedEntityBase
    {
        public int ID { get; protected set; }
    }
}