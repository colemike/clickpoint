﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ClickPoint.Domain.Models
{
    public class User : KeyedEntityBase
    {
        public User(string userName, string email, string firstName, string lastName)
            : this()
        {
            UserName = userName;
            Email = email;
            FirstName = firstName;
            LastName = lastName;
        }
        private User()
        {
            Leads = new Collection<Lead>();
        }

        public string UserName { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public virtual ICollection<Lead> Leads { get; private set; }
    }
}