﻿namespace ClickPoint.Domain.Models
{
    public enum LeadStatus
    {
        Won,
        Hot,
        Cold,
        Lost
    }
}