﻿using WebMatrix.WebData;

namespace ClickPoint.App_Start
{
    public static class MembershipConfig
    {
        public static void RegisterMembership()
        {
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("ClickPoint", "Users", "ID", "UserName", true);
            }
        }
    }
}