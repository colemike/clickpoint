﻿using System;

namespace ClickPoint.Infrastructure
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string value)
        {
            return String.IsNullOrEmpty(value);
        }

        public static string FormatWith(this string value, params object[] args)
        {
            return String.Format(value, args);
        }
    }
}