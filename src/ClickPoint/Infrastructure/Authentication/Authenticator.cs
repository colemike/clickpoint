using System.Linq;
using System.Web.Security;
using ClickPoint.Data;
using ClickPoint.Domain.Models;
using WebMatrix.WebData;

namespace ClickPoint.Infrastructure.Authentication
{
    public class Authenticator : IAuthenticator
    {
        private readonly DataContext _dataContext;

        public Authenticator(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public bool Verify(string username, string password)
        {
            return Membership.ValidateUser(username, password);
        }

        public void Authenticate(string username, string password)
        {
            WebSecurity.Login(username, password);
        }

        public void Logout()
        {
            WebSecurity.Logout();
        }

        public void CreateAccount(string username, string password)
        {
            WebSecurity.CreateAccount(username, password);
        }

        public User GetCurrentUser()
        {
            if (!WebSecurity.IsAuthenticated)
            {
                return null;
            }

            return _dataContext.Users
                               .SingleOrDefault(user => user.UserName == WebSecurity.CurrentUserName);
        }
    }
}