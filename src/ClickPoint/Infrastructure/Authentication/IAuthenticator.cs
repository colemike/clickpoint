using ClickPoint.Domain.Models;

namespace ClickPoint.Infrastructure.Authentication
{
    public interface IAuthenticator
    {
        bool Verify(string username, string password);
        void Authenticate(string username, string password);
        void Logout();
        void CreateAccount(string username, string password);
        User GetCurrentUser();
    }
}