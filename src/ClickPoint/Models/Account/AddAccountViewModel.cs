using System.ComponentModel.DataAnnotations;

namespace ClickPoint.Models.Account
{
    public class AddAccountViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Verify Password")]
        [Compare("Password", ErrorMessage = "Passwords do not match.")]
        public string VerifyPassword { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Email not valid.")]
        public string Email { get; set; }
    }
}