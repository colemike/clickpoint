using System.ComponentModel.DataAnnotations;

namespace ClickPoint.Models.Account
{
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public bool HasLoginFailed { get; set; }

        public bool WasUserCreated { get; set; }
    }
}