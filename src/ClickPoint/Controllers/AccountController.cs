﻿using System.Linq;
using System.Web.Mvc;
using ClickPoint.Data;
using ClickPoint.Domain.Models;
using ClickPoint.Infrastructure;
using ClickPoint.Infrastructure.Authentication;
using ClickPoint.Models.Account;

namespace ClickPoint.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthenticator _authenticator;
        private readonly DataContext _dataContext;

        public AccountController(IAuthenticator authenticator, DataContext dataContext)
        {
            _authenticator = authenticator;
            _dataContext = dataContext;
        }

        public ActionResult Login(bool created = false)
        {
            var loginViewModel = new LoginViewModel
            {
                WasUserCreated = created
            };

            return View(loginViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!_authenticator.Verify(model.Username, model.Password))
            {
                model.HasLoginFailed = true;
                return View(model);
            }

            _authenticator.Authenticate(model.Username, model.Password);

            if (returnUrl.IsNullOrEmpty() || returnUrl == "/")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return Redirect(returnUrl);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult LogOut()
        {
            _authenticator.Logout();

            return RedirectToAction("Login");
        }

        public ActionResult Add()
        {
            return View(new AddAccountViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddAccountViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (_dataContext.Users.Any(u => u.UserName == model.Username))
            {
                ModelState.AddModelError("Username", "Username already taken. Please choose another.");
                return View(model);
            }

            if (_dataContext.Users.Any(u => u.Email == model.Email))
            {
                ModelState.AddModelError("Email", "User already exists for this Email. You should use the imaginary Forgot Your Password link.");
                return View(model);
            }

            var user = new User(model.Username, model.Email, model.FirstName, model.LastName);
            _dataContext.Users.Add(user);
            _dataContext.SaveChanges();

            _authenticator.CreateAccount(model.Username, model.Password);

            return RedirectToAction("Login", new { Created = true });
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Add(AddAccountViewModel model)
        //{
        //    var user = new User(model.Username, model.Email, model.FirstName, model.LastName);
        //    _dataContext.Users.Add(user);
        //    _dataContext.SaveChanges();

        //    return RedirectToAction("Login", new { Created = true });
        //}
    }
}