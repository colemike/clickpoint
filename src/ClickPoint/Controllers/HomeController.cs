﻿using System.Web.Mvc;

namespace ClickPoint.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}