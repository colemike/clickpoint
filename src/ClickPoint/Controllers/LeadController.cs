﻿using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using ClickPoint.Data;
using ClickPoint.Domain.Models;
using ClickPoint.Infrastructure;
using ClickPoint.Infrastructure.Authentication;

namespace ClickPoint.Controllers
{
    [Authorize]
    public class LeadController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly IAuthenticator _authenticator;

        public LeadController(DataContext dataContext, IAuthenticator authenticator)
        {
            _dataContext = dataContext;
            _authenticator = authenticator;
        }

        public ActionResult NewLead()
        {
            var currentUser = _authenticator.GetCurrentUser();

            Lead newLead;

            var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.RepeatableRead });

            using (scope)
            {

                newLead = _dataContext.Leads
                                          .Where(lead => lead.Processor == null)
                                          .OrderBy(lead => lead.EnteredOn)
                                          .FirstOrDefault();

                if (newLead != null)
                {
                    newLead.Processor = currentUser;

                    _dataContext.SaveChanges();
                }

                scope.Complete();
            }

            if (newLead == null)
            {
                return Content("No new leads available.");
            }

            return Content("New Lead: {0} {1}".FormatWith(newLead.FirstName, newLead.LastName));
        }
    }
}