﻿using ClickPoint.Domain.Models;
using Xunit;

namespace ClickPoint.Domain.Tests.Model
{
    public class UserTests
    {
        [Fact]
        public void User_CtorParmsMapCorrectly()
        {
            //Act
            var user = new User("foo", "bar", "baz", "blah");

            //Assert
            Assert.Equal("foo", user.UserName);
            Assert.Equal("bar", user.Email);
            Assert.Equal("baz", user.FirstName);
            Assert.Equal("blah", user.LastName);
        }

        [Fact]
        public void User_HasLeadsInitializedUponCreation()
        {
            //Act
            var user = new User("foo", "bar", "baz", "blah");

            //Assert
            Assert.NotNull(user.Leads);
        }
    }
}