﻿using System.Data.Entity.ModelConfiguration;
using ClickPoint.Domain.Models;

namespace ClickPoint.Data.Infrastructure.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(user => user.ID);

            Property(user => user.UserName).HasMaxLength(50).IsRequired();
            Property(user => user.FirstName).HasMaxLength(50).IsRequired();
            Property(user => user.LastName).HasMaxLength(50).IsRequired();
            Property(user => user.Email).HasMaxLength(100).IsRequired();
        }
    }
}