using System.Data.Entity.ModelConfiguration;
using ClickPoint.Domain.Models;

namespace ClickPoint.Data.Infrastructure.Configurations
{
    public class LeadConfiguration : EntityTypeConfiguration<Lead>
    {
        public LeadConfiguration()
        {
            HasKey(lead => lead.ID);

            Property(lead => lead.FirstName).HasMaxLength(50);
            Property(lead => lead.LastName).HasMaxLength(50);
            Property(lead => lead.Email).HasMaxLength(100);
            Property(lead => lead.Telephone).HasMaxLength(20);

            HasOptional(lead => lead.Processor)
                .WithMany(proc => proc.Leads);
        }
    }
}