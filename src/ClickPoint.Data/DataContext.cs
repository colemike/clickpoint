using ClickPoint.Data.Infrastructure.Configurations;
using ClickPoint.Domain.Models;

namespace ClickPoint.Data
{
    using System.Data.Entity;

    public class DataContext : DbContext
    {
        public DataContext()
            : base("name=ClickPoint") { }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new LeadConfiguration());
        }
    }
}