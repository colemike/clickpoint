namespace ClickPoint.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CreateLead : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Leads",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Email = c.String(maxLength: 100),
                        Telephone = c.String(maxLength: 20),
                        Comments = c.String(),
                        EnteredOn = c.DateTime(nullable: false),
                        Status = c.Int(),
                        Processor_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.Processor_ID)
                .Index(t => t.Processor_ID);

            Sql("INSERT INTO Leads(FirstName, LastName, Email, Telephone, Comments, EnteredOn) VALUES ('Gob', 'Bluth', 'gob@bluthcompany.com', '1231231234', 'He should be an easy sell. Likes magic.', '2014-09-09 20:34:13.520');");
            Sql("INSERT INTO Leads(FirstName, LastName, Email, Telephone, Comments, EnteredOn) VALUES ('Michael', 'Bluth', 'michael@bluthcompany.com', '1231231234', 'Very nice guy. Seems interested but I''m guessing he has money problems.', '2014-09-10 20:34:13.520');");
            Sql("INSERT INTO Leads(FirstName, LastName, Email, Telephone, Comments, EnteredOn) VALUES ('Peter', 'Venkman', 'peter@ghostbusters.com', '4563214567', 'I don''t think this is likely a solid lead.', '2014-09-03 20:34:13.520');");
            Sql("INSERT INTO Leads(FirstName, LastName, Email, Telephone, Comments, EnteredOn) VALUES ('Homer', 'Simpson', 'homer@sprinfieldpowerplant.com', '8887776666', 'Bring donuts before meeting.', '2014-09-01 20:34:13.520');");
            Sql("INSERT INTO Leads(FirstName, LastName, Email, Telephone, Comments, EnteredOn) VALUES ('Walter', 'White', 'walt@heisenberg.com', '4448882222', 'Please don''t mess around with this guy. Don''t go alone. Don''t knock, he likes to do that.', '2014-09-01 20:34:13.520');");
            Sql("INSERT INTO Leads(FirstName, LastName, Email, Telephone, Comments, EnteredOn) VALUES ('Tony', 'Stark', 'tony@starkindustries.com', '1119998808', 'Very interested in buying. Has lots of capital.', '2014-09-06 20:34:13.520');");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Leads", "Processor_ID", "dbo.Users");
            DropIndex("dbo.Leads", new[] { "Processor_ID" });
            DropTable("dbo.Leads");
        }
    }
}
