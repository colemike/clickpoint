using System.Web.Security;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace ClickPoint.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddMembership : DbMigration
    {
        public override void Up()
        {
            WebSecurity.InitializeDatabaseConnection("ClickPoint", "Users", "ID", "UserName", true);
            WebSecurity.CreateUserAndAccount("mike", "password1", new { FirstName = "Mike", LastName = "Cole", Email = "cole.mike@gmail.com" });
        }

        public override void Down()
        {
            //WebSecurity.InitializeDatabaseConnection("ClickPoint", "Users", "ID", "UserName", true);
            //Membership.DeleteUser("mike", true);
            DropTable("webpages_Membership");
            DropTable("webpages_OAuthMembership");


            Sql("IF OBJECT_ID('dbo.webpages_UsersInRoles', 'U') IS NOT NULL DROP TABLE webpages_UsersInRoles");
            Sql("IF OBJECT_ID('dbo.webpages_Roles', 'U') IS NOT NULL DROP TABLE webpages_Roles");
        }
    }
}
