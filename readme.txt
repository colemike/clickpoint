To run application in Visual Studio:

1) Create a new SQL Server database that is accessible.
2) Change the connection string in web.config in the ClickPoint web project to point to the database.
3) Build.
4) In the NuGet Package Manager Console, select the ClickPoint.Data project as the Default Project, and run update-database on the command line. This will initialize the database.